// uni-app请求封装
/*
*router       String  请求接口
*data/param   Object  请求参数
*methods      String  请求方法
*type         Number  提示窗口提示 0请求前后不提示  1请求前提示  2请求后提示 3请求前后提示
*bMsg         String  提示窗口请求前提示语
*eMsg		  String  提示窗口请求后提示语
*errMsg       String  提示窗口错误提示语
*back		  Boolean 请求结束是否返回
*backTime	  Number  请求结束是否返回等待时间
*hideLoading  Boolean 是否隐藏加载
*hasToken     Boolean 是否需要token
*/
import enviroment from "./enviroment.js"
export default class Request {
	http (data) {
		data.backTime = data.backTime == undefined ? 2000 : data.backTime
		let path = enviroment.getUrl();
		
		if(data.type == 1 || data.type == 3){
			uni.showLoading({title:data.bMsg != undefined ? data.bMsg : data.back ? "提交中" : "请求中"  ,mask:true})
		}
		let header = {"Content-Type":"application/json;charset=UTF-8"};
		// 返回promise
		return new Promise((resolve,reject) => {
			// 请求
			uni.request({
				url: `${path}${data.url}`,
				data: data.data || data.param,
				header,
				method:data.methods,
				success: (res) => {
					data.res = res
					console.log("************************我的请求参数***********************")
					console.log(data)
					if(data.hideLoading !== false || data.type !== 0){uni.hideLoading();}
					// 将结果抛出
					if(res.data.code == -401 && data.type != 2 && data.type != 3){
						uni.showToast({title:res.data.msg,icon:"none"})
					}else if(res.data.code == 200){
						resolve(res.data)
						if(data.type == 2 || data.type == 3){
							if(data.back){
								uni.showToast({
									title:data.eMsg || "提交成功",
									icon:"none",
									mask:true,
									duration:1000,
									success(res) {
										setTimeout(function(){
											uni.navigateBack({delta:data.delta || 1})
										},1000)
									}
								})
							}else{
								uni.showToast({title:data.eMsg || "请求成功",icon:"none"})
							}
						}
					}else if(res.data.code == 401){
						uni.showModal({
							title:"温馨提示",
							content:"用户未登录，请登录后操作，是否登录？",
							showCancel:false,
							confirmText:"去登录",
							success: (res) => {
								if(res.confirm){
									uni.reLaunch({
										url:"/pages/login/login"
									})
								}
							}
						})
					}else{
						uni.hideLoading();
						uni.showToast({title:data.errMsg || res.data.msg,icon:"none"})
					}
				},
				fail:(error) =>{
					uni.hideLoading();
					reject(error)
					uni.showToast({title: '网络出小差了~',icon:"none"});
				}
			})
		})
	}
}