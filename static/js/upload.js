
export default class UploadFile {
	http (data) {
		var arr = data.data
		uni.showLoading({title: '图片上传中',mask:true})
		var upload = true;  //是否上传成功状态
		var imgArr = [];    //上传完成后临时存储数组
		
		var path = "http://ctcs.majiangyun.com";
		
		return new Promise((resolve,reject) => {
			if (arr.length == 0){
			  resolve(imgArr)
			}else{
			  var timer = setInterval(function () {
			    if (upload && imgArr.length <= arr.length) {
			      upload = false;
			      uni.uploadFile({
			        url: `${path}${data.url}`,
			        filePath: arr[imgArr.length],
			        name: 'file',
			        success(res) {
			          if (res.statusCode == 200) {
			            upload = true;
			            res.data = JSON.parse(res.data)
			            imgArr.push(res.data.url);
			            if (imgArr.length == arr.length) {
			              clearInterval(timer);
			              uni.hideLoading();
						  uni.showToast({title: '图片上传成功',icon:"none"})
			              resolve(imgArr)
			            }
			          }else{
			            uni.hideLoading();
			            clearInterval(timer);
			            uni.showToast({title: '图片上传失败',icon:"none"})
			          }
			        },fail(err){
						console.log(err)
					}
			      })
			    }
			  }, 100)
			}
		})
	}
}