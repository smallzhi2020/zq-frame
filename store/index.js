import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		navbar:{
			navbarHeight: 0,
			boxHeight:0,
			hasBubble:false,
			oldTop:0,
			hideNavbar:false
		},
		location:{
			latitude:39.908653458973696,
			longitude:116.39747569514947,
			city:"北京",
		},
		userInfo:null
	},
	getters:{
		getNavbarHeight(state,provider){//获取设备高度
			return state.navbar
		},
		getUserInfo(state){
			return state.userInfo;
		},
		getLoaction(state){
			return state.location;
		},
	},
	mutations: {
		SETLOCATION(state,location){//设置地理位置
			state.location = location;
			uni.setStorageSync("location",location);
		},
		setSystemInfo(state,provider){//设置设备信息
			uni.getSystemInfo({
				success:res=> {
					// #ifdef MP-WEIXIN
						state.navbar.hasBubble = true
					// #endif
					// #ifndef MP-WEIXIN
						state.navbar.hasBubble = false
					// #endif
					state.navbar.navbarHeight = res.safeArea.top * 750 / res.screenWidth;
					state.navbar.navbarTitleHieght = 40 * 750 / res.screenWidth;
					state.navbar.boxHeight = res.safeArea.height;
				}
			})
		},
		setNavbarStatus(state,provider){
			state.navbar.hideNavbar = state.navbar.oldTop - provider < 0 && state.navbar.oldTop > state.navbar.navbarHeight + 88;
			state.navbar.oldTop = provider;
		},
		SETUSERINFO(state,userinfo){
			state.userInfo = userinfo;
		}
	},
	actions: {
		
	}
})

export default store
