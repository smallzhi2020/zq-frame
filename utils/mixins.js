export const listData = {
  data() {
    return {
	  initNumberLen:null,
	  timer:null,
      list:[],
      loading:true,
      total:0,
	  loadEnd:false,
      queryParams:{
      	page:1,
      	limit:10
      }
    }
  },
  created(){
	  this.initNumberLen = this.$api.initNumberLen
  },
  onUnLoad(){
	  if(this.timer)clearInterval(this.timer)
  },
  methods:{
	pushParams(name,obj){
		this[name] = Object.assign(this[name],obj);
	},
	reset(type){
		let _this = this;
		return new Promise(function(resolve, reject) {
			if(type == 1){
			  	_this.list = [];
			  	_this.loading = true;
				_this.loadEnd = false;
			  	_this.queryParams.page = 1;
			}
			if(!_this.loading)reject(false)
			resolve(true)
		})
	},
	push(list,total){
		this.total = total;
		this.queryParams.page ++;
		if(this.list.length + list.length >= total)this.loading = false;
		this.loadEnd = true;
		this.timer = setInterval(res =>{
			if(list.length > 0){
				this.list =	this.list.concat(list.splice(0,1));
			}else{
				clearInterval(this.timer)
			}
		},50)
	}
  }
}

export const scrollTop = {
	data(){
		return{
			scrollTop:0
		}
	},
	onPageScroll(res) {
		this.scrollTop = res.scrollTop;
	},
}