# zq-sudoku
## 组件使用说明

- 兼容微信小程序、安卓(ios没测试)、h5
- 该组件全部使用的`rpx`单位
- 劳烦各位大佬点个收藏  [git地址](https://gitee.com/smallzhi2020/zq-frame)

- template代码：

```
	<zq-sudoku :list="sudokuList">
		<template v-slot:item="{item}">全部自定义</template>
		<template v-slot:custom="{item}">指定自定义</template>
		<template v-slot:dot="{item}">全部dot自定义</template>
		<template v-slot:dotCustom="{item}">部分dot自定义</template>
	</zq-sudoku>
	
	data(){
		return{
			sudokuList:[
				{name:"气泡按钮",icon:"/static/images/icon/button.png",custom:true},
				{name:"分栏导航栏",icon:"/static/images/icon/subfield.png",dot:500},
				{name:"加载动画",icon:"/static/images/icon/load.png",dotCustom:true,dot:500},
				{name:"弹窗",icon:"/static/images/icon/pop.png"},
				{name:"九宫格",icon:"/static/images/icon/sudoku.png"},
				{name:"导航栏",icon:"/static/images/icon/navbar.png"},
				{name:"view盒子",icon:"/static/images/icon/box.png"},
			]
		}
	}
```

## 组件属性的全局配置

- 在main.js文件中定义$store

```
import store from './store';
Vue.prototype.$store = store;
```

- 在store的文件中添加以下内容，不使用的属性可不配置,单页面可通过`:props`设置

```
	const store = new Vuex.Store({
		state: {
			zqSudokuSettings:{
				width:750,//盒子的宽度
				margin:"",//盒子外边距
				background:'#ffffff',//盒子的背景颜色
				column:3,//格子的数量
				borderWidth:6,//边框的宽度
				borderColor:"#dddddd",//边框颜色
				interval:20,//格子的间距
				itemWidth:0,//格子的宽度
				itemHeight:200,//格子高度
				fontSize:24,//字号大小
				color:"#333333",//字号颜色
				imgMode:'aspectFit',//图片展示方式
				imgWidth:100,//图片大小
				imgHeight:100,//图片高度
				imgInterval:20,//图片和文字的间距
				delay:0.2,//动画延时
				isMove:true,//是否有动画
				isActive:true,//是否有点击效果
				activeColor:"#eeeeee",//点击颜色
				
				dotFontSize:24,//提示点的字号大小
				dotHeight:40,//提示点的高度
				dotBackground:"#ff414d",//提示点的背景色
				dotColor:"#ffffff",//提示点的字体颜色
				dotTop:15,//提示点的top
				dotRight:50//提示点的right
			}
		}
	})
```

## 组件基本API参数

|属性名|类型|默认值|必填|说明|
| -- | -- | -- | -- | -- |
|list|Array|[]|否|组件列表数据|
|props|Object|null|否|设置九宫格布局的属性|

## props的参数

|属性名|类型|默认值|必填|说明|
| -- | -- | -- | -- | -- |
|width|Number|750|否|九宫格的大小|
|margin|String|""|否|九宫格的外边距，写法同`css`|
|background|String|#ffffff|否|九宫格的背景色，写法同`css`|
|column|Number|3|否|九宫格的列数|
|borderWidth|Number|6|否|九宫格的格子边框的宽度|
|borderColor|String|#dddddd|否|九宫格的格子边框颜色|
|interval|Number|20|否|九宫格的格子边框的间距，borderWidth大于0时，interval设为-borderWidth/2即可无间隙|
|itemWidth|Number|0|否|九宫格的格子宽度，如果不设置自动计算|
|itemHeight|Number|200|否|九宫格的格子高度|
|fontSize|Number|28|否|九宫格的默认字号大小，不影响自定义的字号大小|
|color|String|#333333|否|九宫格的默认字体颜色，不影响自定义的字体颜色|
|imgMode|String|aspectFit|否|九宫格的默认图片展示方式，不影响自定义的图片展示方式|
|imgWidth|Number|100|否|九宫格的图片宽度，不影响自定义的图片宽度|
|imgHeight|Number|100|否|九宫格的图片高度，不影响自定义的图片高度|
|imgInterval|Number|20|否|九宫格的图片和文字的间距，不影响自定义的图片和文字的间距|
|delay|Number|0.2|否|九宫格item的加载动画延时|
|isMove|Boolean|true|否|是否使用九宫格item的加载动画|
|isActive|Boolean|true|否|是否使用九宫格item的点击特效|
|activeColor|String|#eeeeee|否|九宫格item的点击特效颜色|
|dotFontSize|Number|24|否|九宫格提示点的字号大小|
|dotHeight|Number|40|否|九宫格提示点的最小直径|
|dotBackground|String|#ff414d|否|九宫格提示点的背景颜色|
|dotColor|String|#ffffff|否|九宫格提示点的字体颜色|
|dotTop|Number|15|否|九宫格提示点的相对item的top,可通过list里面单独控制|
|dotRight|Number|50|否|九宫格提示点的相对item的right,可通过list里面单独控制|

## list的参数

|属性名|类型|默认值|必填|说明|
| -- | -- | -- | -- | -- |
|name|String|null|否|九宫格标题|
|icon|String|null|否|九宫格的图片链接|
|dot|String,Number|null|否|提示点的数据|
|url|String|null|否|按钮的页面跳转链接，如果需要自定义请使用其他字段即可|
|imgWidth|Number|null|否|针对指定九宫格的图片的宽度|
|imgHeight|Number|null|否|针对指定九宫格的图片的高度|
|fontSize|Number|null|否|针对指定九宫格的文字字体大小|
|imgInterval|Number|null|否|针对指定九宫格的文字和图片的间距|
|dotTop|Number|null|否|针对指定九宫格的指示点相对item的top|
|dotRight|Number|null|否|针对指定九宫格的指示点相对item的right|
|custom|Boolean|false|否|针对指定九宫格item自定义|
|dotCustom|Boolean|false|否|针对指定九宫格指示点自定义|


## 组件事件
|事件名|说明|返回值|
| --| --| --|
|@click|点击事件|item，index|