# zq-box
## 组件使用说明

- 该组件兼容微信小程序、app
- 该组件全部使用的`rpx`单位
- 该组件只测试在view里面使用，暂时未测试scroll-view
- 劳烦各位大佬点个收藏  [git地址](https://gitee.com/smallzhi2020/zq-frame)

- template代码：

```
	<view class="flex_row flex_wrap flex_start">
		<block v-for="(item,index) in list">
			<zq-box v-model="scrollTop">
				<view :style="{'height': item.heigth + 'rpx'}">
					{{index}}
				</view>
			</zq-box>
		</block>
	</view>
	
	data(){
		return{
			list:[]
		}
	}
	
	created() {
		setTimeout(()=>{
			this.timer = setInterval(()=>{
				if(this.list.length < 50){
					this.list.push({heigth:100 + Math.random() * 300,width:100 + Math.random() * 100});
				}else{
					clearInterval(this.timer)
				}
			},100)
		},200)
	}
```

## 组件属性的全局配置

- 在main.js文件中定义$store

```
import store from './store';
Vue.prototype.$store = store;

Vue.mixin({
	data(){
		return{
			scrollTop:0
		}
	},
	on/Page/Scroll(event) {
		this.scrollTop = event.scrollTop;
	}
})
```

- 在store的文件中添加以下内容，不使用的属性可不配置,单页面可通过`:props`设置

```
	const store = new Vuex.Store({
		state: {
			zqBoxSettings:{
				position:'relative',//盒子的定位
				width:710,//盒子的宽度
				height:null,//盒子的高度
				background:"#FFFFFF",//盒子的背景颜色
				padding:"20rpx",//盒子的内边距
				marginLeft:null,//盒子的左外边距
				marginTop:null,//盒子的上外边距
				borderRadius:16,//盒子的圆角
				boxShadow:'0 0 20rpx rgba(0,0,0,0.2)',//盒子的阴影
				isClickMove:true,//是否使用点击动画
				isMove:true,//是否使用动画
				duration:0.6,//动画时长
				showBottom:30,//距离底部多少开始展示
				boxTop:null,//盒子的当前高度
			}
		}
	})
```


## 组件基本API参数

|属性名|类型|默认值|必填|值|说明|
| -- | -- | -- | -- | -- | -- |
|v-model|Number|0|否|0|当前页面滚动的高度，不赋值时且props.isMove == true只在加载时有动画|
|moveMode|String|scale|否|leftRotate向左旋转  rightRotate向右旋转  scale放大 top向上 left向左 right向右 opacity透明 topShake向上震动|盒子加载动画|
|props|Object|null|否|无|盒子配置项|

## props的参数

|属性名|类型|默认值|必填|说明|
| -- | -- | -- | -- | -- |
|background|String|#FFFFFF|否|盒子背景色|
|position|String|"relative"|否|盒子position值|
|width|Number|710|否|盒子的宽度|
|height|Number|null|否|盒子的高度，`不赋值自动撑开，赋值超过部分隐藏`|
|padding|String|20rpx|否|盒子的内边距,同`css`写法|
|marginLeft|Number|null|否|盒子的左外边距,不设值则根据宽度页面居中|
|marginTop|Number|20|否|盒子的上外边距|
|borderRadius|Number|16|否|盒子的圆角|
|boxShadow|String|0 0 20rpx rgba(0,0,0,0.2)|否|盒子的阴影,同`css`写法|
|isClickMove|Boolean|true|否|是否使用点击动画|
|isMove|Boolean|true|否|是否使用加载动画|
|duration|Number|0.6|否|加载动画时长|
|showBottom|Number|30|否|距离页面底部多少开始展示|
|boxTop|Number|null|否|盒子的当前的高度，不赋值自动计算|