# zq-subfield
## 组件使用说明

- 该组件兼容微信小程序、app
- 该组件全部使用的`rpx`单位
- 劳烦各位大佬点个收藏  [git地址](https://gitee.com/smallzhi2020/zq-frame)

- template代码：

```
	<zq-subfield v-model="subfieldIndex1" :list="[{name:'测试一'},{name:'测试一'},{name:'测试一'},{name:'测试一'}]"></zq-subfield>
	<zq-subfield v-model="subfieldIndex2" :list="[{name:'测试一',dotText:'test'},{name:'测试一'},{name:'测试一',dotText:'20'},{name:'测试一'}]"></zq-subfield>
	<zq-subfield v-model="subfieldIndex3" :list="[{name:'测试一',dotText:'test'},{name:'测试一',dotIamge:'/static/images/banner.png',dotIamgeWidth:30,dotIamgeHeight:30},{name:'测试一',dotText:'20'},{name:'测试一',dotIamge:'/static/images/banner.png',dotIamgeWidth:50,dotIamgeHeight:50,dotRight:0,dotTop:25}]"></zq-subfield>
	
	data(){
		return{
			subfieldIndex1:0,
			subfieldIndex2:2,
			subfieldIndex3:3,
		}
	}
```

## 组件属性的全局配置

- 在main.js文件中定义$store

```
import store from './store';
Vue.prototype.$store = store;
```

- 在store的文件中添加以下内容，不使用的属性可不配置,单页面可通过`:props`设置

```
	const store = new Vuex.Store({
		state: {
			zqSubfieldSetting:{
				background:"#FFFFFF",//分栏背景色
				position:"",//分栏定位
				borderBottom:"2rpx solid #F8F8F8",//底部边框
				width:750,//分栏宽度
				height:100,//分栏高度
				top:0,//顶部边距
				left:0,//左边距
				zIndex:999,//层级
				fontSize:24,//分栏文字大小rpx
				color:"#333333",//分栏文字颜色
				activeColor:"#FA8743",//激活文字颜色
				activeFontSize:30,//激活文字大小
				padding:60,//分栏文字间距
				sliderWidth:60,//滑块宽度
				sliderHeight:4,//滑块高度
				sliderBackground:"linear-gradient(to right,#FA8743,#FA8743)",//滑块背景色
				dotFontSize:18,//提示字号大小
				dotBackground:"#DD524D",//提示背景颜色
				dotColor:"#ffffff",//提示文字的颜色
				dotRadius:20//数字提示背景半径
			}
		}
	})
```


## 组件基本API参数

|属性名|类型|默认值|必填|值|说明|
| -- | -- | -- | -- | -- | -- |
|v-model|Number|0|否|0|当前分栏的页码|
|list|Array|[]|否|[]|分栏子内容|
|props|Object|null|否|无|分栏配置项|

## props的参数

|属性名|类型|默认值|必填|说明|
| -- | -- | -- | -- | -- |
|background|String|#FFFFFF|否|分栏背景色|
|position|String|""|否|分栏position值|
|borderBottom|String|2rpx solid #F8F8F8|否|分栏底部边框|
|width|Number|750|否|分栏的宽度|
|height|Number|100|否|分栏的高度|
|top|Number|0|否|顶部边距|
|left|Number|0|否|左边距|
|zIndex|Number|999|否|层级|
|fontSize|Number|24|否|分栏文字大小rpx|
|color|String|#333333|否|分栏文字颜色|
|activeColor|String|#FA8743|否|激活文字颜色|
|activeFontSize|Number|30|否|激活文字大小rpx|
|padding|Number|60|否|分栏文字间距|
|sliderWidth|Number|60|否|滑块宽度|
|sliderHeight|Number|4|否|滑块高度|
|sliderBackground|String|linear-gradient(to right,#FA8743,#FA8743)|否|滑块背景色|
|dotFontSize|Number|18|否|提示字号大小|
|dotBackground|String|#DD524D|否|提示背景颜色|
|dotColor|String|#DD524D|否|提示文字的颜色|
|dotRadius|Number|20|否|数字提示背景半径|


## list的参数

- name为必传属性，如果不传其他属性默认使用内置属性
- initBtn长度大于2的时候，全部通过...隐藏，可点击...展开查看

|属性名|类型|默认值|必填|说明|
| -- | -- | -- | -- | -- |
|name|String|""|否|按钮名称|
|dotText|String|""|否|提示文字|
|dotIamge|String|""|否|提示的图片src|
|dotIamgeWidth|Number|dotRadius * 2|否|提示的图片的宽度|
|dotIamgeHeight|Number|dotRadius * 2|否|提示的图片的高度|
|dotRight|Number|0|否|提示的右边距|
|dotTop|Number|0|否|提示的底边距|


## 组件事件
|事件名|说明|返回值|
| --| --| --|
|@change|点击事件|currentPage,item|