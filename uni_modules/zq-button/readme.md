# zq-button
## 组件使用说明

- 该组件全部使用的`rpx`单位
- 劳烦各位大佬点个收藏  [git地址](https://gitee.com/smallzhi2020/zq-frame)

- template代码：

```
	<zq-button color="red" margin="20rpx 0 0 20rpx">red</zq-button>
	<zq-button color="orange" margin="20rpx 0 0 20rpx">orange</zq-button>
	<zq-button color="yellow" margin="20rpx 0 0 20rpx">yellow</zq-button>
	<zq-button color="green" margin="20rpx 0 0 20rpx">green</zq-button>
	<zq-button color="blue" margin="20rpx 0 0 20rpx">blue</zq-button>
	<zq-button color="cyan" margin="20rpx 0 0 20rpx">cyan</zq-button>
	<zq-button color="violet" margin="20rpx 0 0 20rpx">violet</zq-button>
	
	<zq-button color="pink" margin="20rpx 0 0 20rpx">pink</zq-button>
	<zq-button color="gray" margin="20rpx 0 0 20rpx">gray</zq-button>
	<zq-button color="black" margin="20rpx 0 0 20rpx">black</zq-button>
	<zq-button margin="20rpx 0 0 20rpx">默认色</zq-button>
	<zq-button margin="20rpx 0 0 20rpx" :props="{showBoxShadow:false}">无阴影</zq-button>
	<zq-button margin="20rpx 0 0 20rpx" :props="{boxShadow:'0 0 50rpx green'}">自定义外阴影</zq-button>
	<zq-button margin="20rpx 0 0 20rpx" :props="{insetShadow:50}">内阴影宽度</zq-button>
	<zq-button margin="20rpx 0 0 20rpx" :props="{color:'black',fontSize:20}">字体大小颜色</zq-button>
	<zq-button margin="20rpx 0 0 20rpx" :props="{interval:15}">气泡边隙</zq-button>
	<zq-button margin="20rpx 0 0 20rpx" :props="{borderRadius:'0 30rpx 0 30rpx'}">自定义圆角</zq-button>
	<zq-button margin="20rpx 0 0 20rpx" :props="{width:250,height:80}">自定义宽高</zq-button>
```

## 组件属性的全局配置

- 在main.js文件中定义$store

```
import store from './store';
Vue.prototype.$store = store;
```

- 在store的文件中添加以下内容，不使用的属性可不配置,单页面可通过`:props`设置

```
	const store = new Vuex.Store({
		state: {
			zqButtonSetting:{
				width:350,//按钮宽度
				height:100,//按钮高度
				interval:8,//气泡间隙
				padding:20,//气泡内左右间隔
				insetShadow:10,//内阴影宽度
				boxShadow:"",//外阴影
				showBoxShadow:true,是否显示按钮外阴影
				borderRadius:"12rpx",//圆角
				fontSize:30,//按钮字号
				color:"#ffffff",//字体颜色
				clickDelay:200,//点击延迟毫秒数
				hasAnimation:true//是否有点击动画
			}
		}
	})
```


## 组件基本API参数

|属性名|类型|默认值|必填|值|说明|
| -- | -- | -- | -- | -- | -- |
|color|String|0|否|red pink green blue gray orange yellow violet cyan black|按钮的背景颜色，内置了部分的颜色属性，可自定义颜色|
|props|Object|null|否|无|按钮配置项|
|margin|String|""|否|无|按钮外边距，同css写法`0 0 0 10rpx`|
|type|String|""|否|rectangle,circle|按钮的样式`预留`|

## props的参数

|属性名|类型|默认值|必填|说明|
| -- | -- | -- | -- | -- |
|width|Number|350|否|按钮的宽度|
|height|Number|100|否|按钮的高度|
|interval|Number|8|否|气泡高亮和边框的距离|
|padding|Number|20|否|气泡内左右间隔|
|insetShadow|Number|10|否|内阴影宽度|
|boxShadow|String|""|否|外阴影|
|showBoxShadow|Boolean|true|否|是否显示外阴影|
|borderRadius|String|12rpx|否|按钮的圆角度数,同css写法|
|fontSize|Number|30|否|按钮的字号大小|
|color|String|#ffffff|否|按钮的字体颜色|
|clickDelay|Number|200|否|点击延迟毫秒数|
|hasAnimation|Boolean|true|否|按钮是否使用点击动画|


## 组件事件
|事件名|说明|
| --| --|
|@click|点击事件|